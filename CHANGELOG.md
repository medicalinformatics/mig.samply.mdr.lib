# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.0] - 2020-03-16
### Changed
- apply google java code style
- do not enable regex check as default for string validation

## [2.0.0] - 2019-05-09
### Fixed
- Country code for czech republic changed

## [1.2.0] - 2019-04-24
### Added
- templates for pull requests and issues
- DatatypeField "TBD" (to be defined) to use for drafts
- Enum for the Link type of a dataelement based on the FHIR value set concept-map-equivalence

## [1.1.0] - 2018-12-19
### Added
- Import Source enum
### Changed
- Add date format string to DateRepresentationEnum

## [1.0.0] - 2018-12-17
### Added
- Initial release of the library
