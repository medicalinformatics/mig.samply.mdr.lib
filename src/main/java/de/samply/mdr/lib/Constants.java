/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.lib;

/**
 * Some basic constants and enums that are used in this Application.
 *
 * @author paul
 */
public class Constants {

  /** Represents the source of meta data elements. */
  public enum Source {
    LOCALMDR,
    CADSR;
  }

  /**
   * Represents the select box of validation type in the data element wizard.
   *
   * @author paul
   */
  public enum DatatypeField {
    LIST,
    INTEGER,
    FLOAT,
    BOOLEAN,
    STRING,
    DATE,
    DATETIME,
    TIME,
    CATALOG,
    TBD;

    public String getName() {
      return toString();
    }
  }

  /**
   * Represents the language of a definition.
   *
   * @author paul
   */
  public enum Language {
    EN,
    BG,
    ES,
    CZ,
    DA,
    DE,
    ET,
    EL,
    FR,
    GA,
    HR,
    IT,
    LV,
    LT,
    HU,
    MT,
    NL,
    PL,
    PT,
    RO,
    SK,
    SL,
    FI,
    SV;

    public String getName() {
      return toString().toLowerCase();
    }
  }

  /**
   * Defines the representation of a time value.
   *
   * @author paul
   */
  public enum TimeRepresentation {
    LOCAL_TIME,
    HOURS_24,
    HOURS_12;

    /**
     * TODO: add javadoc.
     */
    public static TimeRepresentation parseValidationData(String input) {
      if (input.contains(";")) {
        input = input.split(";")[1];
      }

      if (input.startsWith("LOCAL_TIME")) {
        return LOCAL_TIME;
      }

      if (input.startsWith("HOURS_24")) {
        return HOURS_24;
      }

      if (input.startsWith("HOURS_12")) {
        return HOURS_12;
      }

      return LOCAL_TIME;
    }

    public String getName() {
      return toString().toLowerCase();
    }
  }

  /**
   * Defines the representation of a date value.
   *
   * @author paul
   */
  public enum DateRepresentation {
    // Used lower d and y to make it compatible with Javas SimpleDateFormat,
    // as that interprets upper D as Day of Year not Day of Month, and upper Y as WeekYear
    LOCAL_DATE("yyyy-MM-dd"),
    ISO_8601("yyyy-MM-dd"),
    DIN_5008("dd/MM/yyyy");

    private String format;

    DateRepresentation(String format) {
      this.format = format;
    }

    /**
     * TODO: add javadoc.
     */
    public static DateRepresentation parseValidationData(String input) {

      String lowerInput = input.toLowerCase();

      for (DateRepresentation representation : DateRepresentation.values()) {
        if (lowerInput.startsWith(representation.getName())) {
          return representation;
        }
      }

      return LOCAL_DATE;
    }

    public String getName() {
      return toString().toLowerCase();
    }

    public String getFormat() {
      return this.format;
    }
  }

  /** Defines where an import originates from. */
  public enum ImportSource {
    MDR_EXPORT,
    EXCEL,
    XML,
    REST;
  }

  /**
   * Represents the link type of a dataelement based on the FHIR value set concept-map-equivalence.
   *
   * @author abishaa
   */
  public enum LinkType {
    UNDEFINED,
    EQUAL,
    EQUIVALENT,
    WIDER,
    SUBSUMES,
    NARROWER,
    SPECIALIZES,
    INEXACT;

    public String getName() {
      return toString().toLowerCase();
    }

    /**
     * TODO: add javadoc.
     */
    public String getDescription(String linkTypeName) {
      String description = null;
      switch (linkTypeName) {
        case "UNDEFINED":
          description = "The link type between the concept and the data element is not specified.";
          break;
        case "EQUAL":
          description =
              "The definitions of the concepts are exactly the same (i.e. only grammatical "
                  + "differences) and structural implications of meaning are identical or "
                  + "irrelevant (i.e. intentionally identical).";
          break;
        case "EQUIVALENT":
          description =
              "The definitions of the concepts mean the same thing (including when structural "
                  + "implications of meaning are considered) (i.e. extensionally identical).";
          break;
        case "WIDER":
          description = "The target mapping is wider in meaning than the source concept.";
          break;
        case "SUBSUMES":
          description =
              "The target mapping subsumes the meaning of the source concept (e.g. the source is-a "
                  + "target).";
          break;
        case "NARROWER":
          description =
              "The target mapping is narrower in meaning than the source concept. The sense in "
                  + "which the mapping is narrower SHALL be described in the comments in this "
                  + "case, and applications should be careful when attempting to use these "
                  + "mappings operationally.";
          break;
        case "SPECIALIZES":
          description =
              "The target mapping specializes the meaning of the source concept (e.g. the target "
                  + "is-a source).";
          break;
        case "INEXACT":
          description =
              "The target mapping overlaps with the source concept, but both source and target "
                  + "cover additional meaning, or the definitions are imprecise and it is "
                  + "uncertain whether they have the same boundaries to their meaning. The sense "
                  + "in which the mapping is inexact SHALL be described in the comments in this "
                  + "case, and applications should be careful when attempting to use these "
                  + "mappings operationally.";
          break;
        default:
          description = "No desciption found!";
          break;
      }
      return description;
    }
  }
}
