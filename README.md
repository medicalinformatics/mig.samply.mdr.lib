# Samply MDR LIB

Internal library used for the samply MDR.

## Build

Use maven to build the `war` file:

```
mvn clean package
```
